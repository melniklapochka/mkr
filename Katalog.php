<?php
include 'Tour.php';

class Katalog
{
    public function tourCount($tours)
    {
        return count($tours);
    }

    public function numberOfWorkersCount($tours, $numberOfWorkers)
    {
        $count = 0;
        foreach ($tours as $tour) {
            if ($tour->numberOfWorkers == $numberOfWorkers) {
                $count++;
            }
        }
        return $count;
    }
}