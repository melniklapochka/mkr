<?php


class Tour
{
    public $name;
    public $numberOfCountry;
    public $numberOfWorkers;

    /**
     * Tour constructor.
     * @param $name
     * @param $numberOfCountry
     * @param $numberOfWorkers
     */
    public function __construct($name, $numberOfCountry, $numberOfWorkers)
    {
        $this->name = $name;
        $this->numberOfCountry = $numberOfCountry;
        $this->numberOfWorkers = $numberOfWorkers;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getNumberOfCountry()
    {
        return $this->numberOfCountry;
    }

    /**
     * @param mixed $numberOfCountry
     */
    public function setNumberOfCountry($numberOfCountry)
    {
        $this->numberOfCountry = $numberOfCountry;
    }

    /**
     * @return mixed
     */
    public function getNumberOfWorkers()
    {
        return $this->numberOfWorkers;
    }

    /**
     * @param mixed $numberOfWorkers
     */
    public function setNumberOfWorkers($numberOfWorkers)
    {
        $this->numberOfWorkers = $numberOfWorkers;
    }
}